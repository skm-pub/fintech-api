FROM python:3.10.9-slim

ARG USER_NAME=fintech
ARG GROUP_NAME=fintech
ARG ROOT_DIR=/main
ENV PYTHONUNBUFFERED 1

EXPOSE 8080

WORKDIR ${ROOT_DIR}

COPY ./alembic /${ROOT_DIR}/alembic
COPY ./alembic.ini /${ROOT_DIR}/alembic.ini
COPY ./requirements.txt /${ROOT_DIR}/requirements.txt
COPY ./app /${ROOT_DIR}/app
COPY ./tests /${ROOT_DIR}/tests
COPY ./makefile /${ROOT_DIR}/makefile
COPY ./.pre-commit-config.yaml /${ROOT_DIR}/.pre-commit-config.yaml

RUN groupadd -r ${GROUP_NAME} \
    && useradd -l -r -g ${USER_NAME} ${GROUP_NAME} \
    && chown -R ${USER_NAME}:${GROUP_NAME} ${ROOT_DIR} \
    && apt -y update \
    && apt -y install make \
    && pip install --upgrade pip \
    && pip install -r ${ROOT_DIR}/requirements.txt \
    && apt-get clean autoclean \
    && apt-get autoremove --yes \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/

USER ${USER_NAME}:${GROUP_NAME}
