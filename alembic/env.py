import asyncio
from logging.config import fileConfig

from sqlalchemy import create_engine, pool, text
from sqlalchemy.engine import Connection
from sqlalchemy.ext.asyncio import AsyncEngine
from yarl import URL

from alembic import context
from app.core.config import Env, MainConfig
from app.database import Base

target_metadata = Base.metadata
app_config = MainConfig()
db_url_main = URL(app_config.db.url_main)
db_url_tpl = URL(app_config.db.url_template)

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)


async def create_db(name):
    _engine = AsyncEngine(
        create_engine(
            url=str(db_url_main),
            poolclass=pool.NullPool,
            execution_options={"isolation_level": "AUTOCOMMIT"},
        )
    )
    async with _engine.connect() as conn:
        db_exists = await conn.execute(
            text(f"SELECT datname FROM pg_database WHERE datname = '{name}'")
        )

        if not db_exists.fetchone():
            await conn.execute(text(f"CREATE DATABASE {name}"))


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def do_run_migrations(connection: Connection) -> None:
    context.configure(connection=connection, target_metadata=target_metadata)

    with context.begin_transaction():
        context.run_migrations()


async def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    main_engine = AsyncEngine(
        create_engine(
            url=str(db_url_main),
            poolclass=pool.NullPool,
        )
    )

    async with main_engine.connect() as connection:
        await connection.run_sync(do_run_migrations)

    await main_engine.dispose()

    if app_config.main.env in [Env.test, Env.dev, Env.debug]:
        await create_db(db_url_tpl.name)

        test_engine = AsyncEngine(
            create_engine(url=str(db_url_tpl), poolclass=pool.NullPool)
        )

        async with test_engine.connect() as connection:
            await connection.run_sync(do_run_migrations)

        await test_engine.dispose()


if context.is_offline_mode():
    run_migrations_offline()
else:
    asyncio.run(run_migrations_online())
