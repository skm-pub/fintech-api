import pytest

from app.core.config import MainConfig
from app.core.test_utils import client
from app.models.web.requests.users import UserIN, UserUpdateIN
from app.models.web.responses.users import UserUpdateOUT
from tests import user_create, user_get

config = MainConfig()
user = UserIN(
    id=None,
    name="Test",
    surname="User",
    middle_name="",
    is_blocked=False,
    is_system=False,
)
system_user = UserIN(
    id=None,
    name="System",
    surname="User",
    middle_name="",
    is_blocked=False,
    is_system=True,
)
endpoint = "/users"


@pytest.mark.asyncio
async def test_user_create_and_get(client):
    user_id = user_create(client, user)
    created = user_get(client, user_id)
    assert created.name == user.name


@pytest.mark.asyncio
async def test_system_user_create_and_get(client):
    user_id = user_create(client, system_user)
    created = user_get(client, user_id)

    assert created.is_system == True


@pytest.mark.asyncio
async def test_user_update_and_get(client):
    user_id = user_create(client, user)
    created = user_get(client, user_id)

    new_name = f"{created.name} (edited)"
    new_surname = f"{created.middle_name} (edited)"
    new_middle_name = f"{created.name} (edited)"

    block_user = False if created.is_blocked else True

    payload = UserUpdateIN(
        data=UserIN(
            id=created.id,
            name=new_name,
            surname=new_surname,
            middle_name=new_middle_name,
            is_blocked=block_user,
            is_system=False,
        )
    )
    res = client.put(f"{endpoint}/{created.id}", json=payload.dict())

    assert res.status_code == 200

    body = UserUpdateOUT.parse_obj(res.json())
    updated = user_get(client, body.id)

    assert updated.name == new_name
    assert updated.surname == new_surname
    assert updated.middle_name == new_middle_name
    assert updated.is_blocked == block_user


@pytest.mark.asyncio
async def test_user_delete(client):
    user_id = user_create(client, user)
    created = user_get(client, user_id)

    res = client.delete(f"{endpoint}/{created.id}")

    assert res.status_code == 200

    exists = user_get(client, created.id)

    assert not exists
