from fastapi.testclient import TestClient

from app.core.config import Env, MainConfig
from app.models.web.requests.users import UserCreateIN, UserIN
from app.models.web.responses.users import UserBodyOUT, UserCreateOUT, UserOUT

config = MainConfig()
USERS_ENDPOINT = "users"


if config.main.env != Env.test:
    raise RuntimeError(
        f"Run test on non-test environment is rejected. Current environment: {config.main.env.name}"
    )


def user_get(tc: TestClient, _user_id) -> UserBodyOUT | None:
    res = tc.get(f"{USERS_ENDPOINT}/{_user_id}")

    match res.status_code:
        case 200:
            body = UserOUT.parse_obj(res.json()).data
        case _:
            body = None

    return body


def user_create(tc: TestClient, _user: UserIN) -> int:
    res = tc.post(USERS_ENDPOINT, json=UserCreateIN(data=_user).dict())
    assert res.status_code == 200

    body = UserCreateOUT.parse_obj(res.json())

    return body.id
