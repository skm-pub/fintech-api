from asyncio import sleep as asleep
from decimal import Decimal

import pytest
from fastapi.testclient import TestClient

from app.core.config import MainConfig
from app.core.test_utils import client
from app.models.web.requests.transactions import (
    TransactionBodyIN,
    TransactionCreateIN,
    TransactionDepositBodyIN,
    TransactionWithdrawBodyIN,
)
from app.models.web.requests.users import UserIN
from tests import user_create, user_get

config = MainConfig()

system_user = UserIN(
    id=None,
    name="System",
    surname="User",
    middle_name="",
    is_blocked=False,
    is_system=True,
)
user_a = UserIN(
    id=None,
    name="User",
    surname="A",
    middle_name="",
    is_blocked=False,
    is_system=False,
)
user_b = UserIN(
    id=None,
    name="User",
    surname="B",
    middle_name="",
    is_blocked=False,
    is_system=False,
)
endpoint = "/transactions"


async def do_deposit(
    tc: TestClient, user_id: int, value: Decimal, attempts: int = 10
) -> bool:
    payload = TransactionCreateIN(
        data=TransactionDepositBodyIN(
            sender=None,
            recepient=user_id,
            value=value,
        )
    )

    res = tc.post(f"{endpoint}/", data=payload.json())

    assert res.status_code == 200

    _user = user_get(tc, user_id)

    while attempts:
        attempts -= 1

        if _user.balance == value:
            return True

        _user = user_get(tc, user_id)

        await asleep(0.3)

    return False


@pytest.mark.asyncio
async def test_deposit(client):
    deposit_value = Decimal(1000)
    user_create(client, system_user)

    user_id = user_create(client, user_a)
    assert user_id, "Unable to create user"

    await do_deposit(client, user_id, deposit_value)

    _user = user_get(client, user_id)
    assert _user.balance == deposit_value, "User balance not equal deposit value"


@pytest.mark.asyncio
async def test_withdraw(client):
    deposit_value = Decimal(1000)
    withdraw_value = Decimal(100)
    user_create(client, system_user)
    attempts = 10

    user_id = user_create(client, user_a)
    assert user_id, "Unable to create user"

    await do_deposit(client, user_id, deposit_value)

    payload = TransactionCreateIN(
        data=TransactionWithdrawBodyIN(
            sender=user_id,
            recepient=None,
            value=withdraw_value,
        )
    )

    res = client.post(f"{endpoint}/", data=payload.json())

    assert res.status_code == 200

    _user = user_get(client, user_id)

    while attempts:
        attempts -= 1

        if _user.balance == deposit_value - withdraw_value:
            return True

        _user = user_get(client, user_id)

        await asleep(0.3)

    assert (
        _user.balance == deposit_value - withdraw_value
    ), "User balance not equal deposit - withdraw"


@pytest.mark.asyncio
async def test_transfer(client):
    deposit_value = Decimal(1000)
    transfer_value = Decimal(100)
    user_create(client, system_user)
    attempts = 10

    user_a_id = user_create(client, user_a)
    assert user_a_id, "Unable to create user"

    user_b_id = user_create(client, user_b)
    assert user_a_id, "Unable to create user"

    await do_deposit(client, user_a_id, deposit_value)

    payload = TransactionCreateIN(
        data=TransactionBodyIN(
            sender=user_a_id,
            recepient=user_b_id,
            value=transfer_value,
        )
    )

    res = client.post(f"{endpoint}/", data=payload.json())

    assert res.status_code == 200

    _user_a = user_get(client, user_a_id)
    _user_b = user_get(client, user_b_id)

    while attempts:
        attempts -= 1

        if (_user_a.balance == deposit_value - transfer_value) and (
            _user_b.balance == transfer_value
        ):
            return True

        _user_a = user_get(client, user_a_id)
        _user_b = user_get(client, user_b_id)

        await asleep(0.3)

    assert _user_a.balance == deposit_value - transfer_value, "Wrong sender balance"
    assert _user_b.balance == transfer_value, "Wrong recipient balance"
