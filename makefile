.PHONY: migrate test app celery

app: migrate serve
test: migrate coverage

migrate:
	@echo "Running migrations"
	alembic upgrade head

serve:
	@echo "Running app"
	uvicorn app.main:app --host 0.0.0.0 --port 8080

celery:
	@echo "Running celery"
	celery -A app.tasks:celery worker -P solo -E -l debug -c 1 -Q $FINTECH_API_DB__CELERY_QUEUE_NAME

coverage:
	@echo "Running tests"
	coverage run -m pytest ./tests || coverage report && coverage json
