## Fintech API

Just simple API built on Fast API

### Local deployment

1. ```cp deployment/docker-compose/docker-compose.sample.yml deployment/docker-compose/docker-compose.yml```
2. Replace variable templates with real values
3. ```docker-compose --verbose --project-directory /path/to/project/root -f deployment/docker-compose/docker-compose.yml up```
4. ```poetry install --with dev # install dependencies```
5. ```poetry shell```
6. ```python debug.py```
