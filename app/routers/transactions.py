from fastapi import APIRouter, Depends, Path
from fastapi.responses import UJSONResponse

from app.core.crud.transactions import TransactionCRUD, tran_crud
from app.models.web.requests.transactions import (
    TransactionCreateIN,
    TransactionDepositBodyIN,
    TransactionWithdrawBodyIN,
)
from app.models.web.responses.transactions import (
    TransactionBodyOUT,
    TransactionCreateErrorOUT,
    TransactionCreateOUT,
    TransactionDeleteErrorOUT,
    TransactionDeleteOUT,
    TransactionListNotFound,
    TransactionListOUT,
    TransactionNotFound,
    TransactionOUT,
)

router = APIRouter(
    prefix="/transactions",
    tags=["transactions"],
)


@router.get(
    "/",
    responses={
        200: {"model": TransactionListOUT},
        404: {"model": TransactionListNotFound},
    },
)
async def _all(
    page: int,
    size: int,
    crud: TransactionCRUD = Depends(tran_crud),
):
    page = await crud.read(page, size)

    if not page.items:
        return UJSONResponse(
            status_code=404, content=TransactionListNotFound(data=page).dict()
        )

    trans = []
    for tran in page.items:
        trans.append(
            TransactionBodyOUT(
                id=tran.id,
                sender=tran.sender,
                recepient=tran.recepient,
                value=tran.value,
                type=tran.type.value,
                status=tran.status.value,
            )
        )

    page.items = trans
    return UJSONResponse(status_code=200, content=TransactionListOUT(data=page).dict())


@router.get(
    "/{id}",
    responses={
        200: {"model": TransactionOUT},
        404: {"model": TransactionNotFound},
    },
)
async def _one(
    tran_id: int = Path(alias="id"),
    crud: TransactionCRUD = Depends(tran_crud),
):
    tran = await crud.read_one(tran_id)

    if not tran:
        return UJSONResponse(status_code=404, content=TransactionNotFound().dict())

    return UJSONResponse(
        TransactionOUT(
            data=TransactionBodyOUT(
                id=tran.id,
                sender=tran.sender,
                recepient=tran.recepient,
                value=tran.value,
                type=tran.type.value,
                status=tran.status.value,
            )
        ).dict(),
        status_code=200,
    )


@router.post(
    "/",
    responses={
        200: {"model": TransactionCreateOUT},
        500: {"model": TransactionCreateErrorOUT},
    },
)
async def _create(
    req_body: TransactionCreateIN
    | TransactionDepositBodyIN
    | TransactionWithdrawBodyIN,
    crud: TransactionCRUD = Depends(tran_crud),
):
    tran_id = await crud.create(req_body.data)

    if not tran_id:
        return UJSONResponse(
            TransactionCreateErrorOUT().dict(),
            status_code=500,
        )

    return UJSONResponse(
        TransactionCreateOUT(id=tran_id).dict(),
        status_code=200,
    )


@router.delete(
    "/{id}",
    responses={
        200: {"model": TransactionDeleteOUT},
        500: {"model": TransactionDeleteErrorOUT},
    },
)
async def _delete(
    tran_id: int = Path(alias="id"),
    crud: TransactionCRUD = Depends(tran_crud),
):
    if not await crud.delete(tran_id):
        return UJSONResponse(TransactionDeleteErrorOUT().dict(), status_code=500)

    return UJSONResponse(TransactionDeleteOUT(id=tran_id).dict(), status_code=200)
