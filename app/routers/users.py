from fastapi import APIRouter, Depends, Path
from fastapi.responses import UJSONResponse

from app.core.crud.users import UserCRUD, user_crud
from app.models.web.requests.users import UserCreateIN, UserUpdateIN
from app.models.web.responses.users import (
    UserBodyOUT,
    UserCreateErrorOUT,
    UserCreateOUT,
    UserDeleteErrorOUT,
    UserDeleteOUT,
    UserListNotFound,
    UserListOUT,
    UserNotFound,
    UserOUT,
    UserUpdateErrorOUT,
    UserUpdateOUT,
)

router = APIRouter(
    prefix="/users",
    tags=["users"],
)


@router.get(
    "/",
    responses={
        200: {"model": UserListOUT},
        404: {"model": UserListNotFound},
    },
)
async def _all(
    page: int,
    size: int,
    crud: UserCRUD = Depends(user_crud),
):
    page = await crud.read(page, size)

    if not page.items:
        return UJSONResponse(
            status_code=404, content=UserListNotFound(data=page).dict()
        )

    users = []
    for user in page.items:
        users.append(
            UserBodyOUT(
                id=user.id,
                name=user.name,
                surname=user.surname,
                middle_name=user.middle_name,
                balance=user.balance,
                is_blocked=user.is_blocked,
                is_system=user.is_system,
            )
        )

    page.items = users
    return UJSONResponse(status_code=200, content=UserListOUT(data=page).dict())


@router.get(
    "/{id}",
    responses={
        200: {"model": UserOUT},
        404: {"model": UserNotFound},
    },
)
async def _one(
    user_id: int = Path(alias="id"),
    crud: UserCRUD = Depends(user_crud),
):
    user = await crud.read_one(user_id)

    if not user:
        return UJSONResponse(status_code=404, content=UserNotFound().dict())

    return UJSONResponse(
        UserOUT(
            data=UserBodyOUT(
                id=user.id,
                name=user.name,
                surname=user.surname,
                middle_name=user.middle_name,
                balance=user.balance,
                is_blocked=user.is_blocked,
                is_system=user.is_system,
            )
        ).dict(),
        status_code=200,
    )


@router.post(
    "/",
    responses={
        200: {"model": UserCreateOUT},
        500: {"model": UserCreateErrorOUT},
    },
)
async def _create(
    req_body: UserCreateIN,
    crud: UserCRUD = Depends(user_crud),
):
    user_id = await crud.create(req_body.data)

    if not user_id:
        return UJSONResponse(
            UserCreateErrorOUT().dict(),
            status_code=500,
        )

    return UJSONResponse(
        UserCreateOUT(id=user_id).dict(),
        status_code=200,
    )


@router.put(
    "/{id}",
    responses={
        200: {"model": UserUpdateOUT},
        500: {"model": UserUpdateErrorOUT},
    },
)
async def _update(
    req_body: UserUpdateIN,
    user_id: int = Path(alias="id"),
    crud: UserCRUD = Depends(user_crud),
):
    req_body.data.id = user_id
    updated_id = await crud.update(req_body.data)

    if not updated_id:
        return UJSONResponse(status_code=500, content=UserUpdateErrorOUT().dict())

    user = await crud.read_one(updated_id)

    if not user:
        return UJSONResponse(status_code=500, content=UserUpdateErrorOUT().dict())

    return UJSONResponse(
        UserUpdateOUT(
            id=user.id,
            data=UserBodyOUT(
                id=user.id,
                name=user.name,
                surname=user.surname,
                middle_name=user.middle_name,
                balance=user.balance,
                is_blocked=user.is_blocked,
                is_system=user.is_system,
            ),
        ).dict(),
        status_code=200,
    )


@router.delete(
    "/{id}",
    responses={
        200: {"model": UserDeleteOUT},
        500: {"model": UserDeleteErrorOUT},
    },
)
async def _delete(
    user_id: int = Path(alias="id"),
    crud: UserCRUD = Depends(user_crud),
):
    if not await crud.delete(user_id):
        return UJSONResponse(UserDeleteErrorOUT().dict(), status_code=500)

    return UJSONResponse(UserDeleteOUT(id=user_id).dict(), status_code=200)
