import logging

import bugsnag
import json_log_formatter
from bugsnag.handlers import BugsnagHandler
from loguru import logger

from app.core.config import MainConfig

config = MainConfig()
bugsnag.configure(
    api_key=config.main.log.bugsnag_api_key,
    release_stage=config.main.env.value,
)

json_formatter = json_log_formatter.JSONFormatter()
json_handler = logging.FileHandler(filename=config.main.log.path)
json_handler.setFormatter(json_formatter)
json_handler.setLevel(logging.INFO)
logger.add(json_handler, enqueue=True)

bugsnag_handler = BugsnagHandler()
bugsnag_handler.setLevel(logging.ERROR)
logger.add(bugsnag_handler, enqueue=True)
