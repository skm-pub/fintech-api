import bugsnag
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import UJSONResponse
from fastapi_pagination import add_pagination
from starlette.requests import Request

from app.core.config import Env, MainConfig
from app.log import logger
from app.models.web import ResponseError
from app.routers import transactions, users

config = MainConfig()
bugsnag.configure(
    api_key=config.main.log.bugsnag_api_key, release_stage=config.main.env.value
)


def get_application():
    _app = FastAPI(
        title=config.main.project_name,
    )

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=[
            str(origin) for origin in config.main.backend_cors_origins.split(",")
        ],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    return _app


async def catch_exceptions_middleware(request: Request, call_next):
    try:
        return await call_next(request)
    except Exception as e:
        logger.exception("Critical error:", exc_info=e)
        return UJSONResponse(
            ResponseError(msg="Internal server error").dict(),
            status_code=500,
        )


app = get_application()
app.include_router(users.router)
app.include_router(transactions.router)
add_pagination(app)

# list of environments with
# enabled exceptions
exc_enabled_envs = [
    Env.test,
    Env.debug,
]
if config.main.env not in exc_enabled_envs:
    app.middleware("http")(catch_exceptions_middleware)


@app.on_event("startup")
async def startup() -> None:
    ...


@app.on_event("shutdown")
async def shutdown() -> None:
    ...
