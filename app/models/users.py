from decimal import Decimal

from sqlalchemy import Boolean, Column, String
from sqlalchemy.orm import relationship

from app.database import Base
from app.models import BALANCE_TYPE, BaseModel


class User(Base, BaseModel):
    __tablename__ = "users"

    name: str = Column(String(256), nullable=False)
    surname: str = Column(String(256), nullable=False)
    middle_name: str = Column(String(256), nullable=True)
    is_blocked: bool = Column(Boolean, default=False, nullable=False)
    is_system: bool = Column(Boolean, default=False, nullable=False)
    balance: Decimal = Column("balance", BALANCE_TYPE, default="0", nullable=False)

    balances = relationship(
        "Balance",
        back_populates="user",
        cascade="all, delete",
    )
