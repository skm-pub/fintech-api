from sqlalchemy import Column, DateTime, Integer, func
from sqlalchemy.dialects.postgresql import NUMERIC

BALANCE_TYPE = NUMERIC(23, 8)


class BaseModel:
    id: int = Column(Integer, primary_key=True, index=True)
    created = Column(DateTime, server_default=func.now())
    updated = Column(DateTime, onupdate=func.now())

    def dict(self):
        return self.__dict__
