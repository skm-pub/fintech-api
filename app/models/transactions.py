from decimal import Decimal
from enum import Enum

from sqlalchemy import Column
from sqlalchemy import Enum as SqlEnum
from sqlalchemy import ForeignKey, Integer

from app.database import Base
from app.models import BALANCE_TYPE, BaseModel


class TransactionType(Enum):
    deposit: int = 0
    withdraw: int = 1
    transfer: int = 2


class TransactionStatus(Enum):
    created: int = 0
    apply: int = 1
    success: int = 2
    error: int = 3
    deleted: int = 4


class Transaction(Base, BaseModel):
    __tablename__ = "transactions"

    sender = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    recepient = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))

    value: Decimal = Column(BALANCE_TYPE, nullable=False)
    type: TransactionType = Column(
        SqlEnum(TransactionType),
        nullable=False,
    )
    status: TransactionStatus = Column(
        SqlEnum(TransactionStatus),
        default=TransactionStatus.created,
        nullable=False,
    )
