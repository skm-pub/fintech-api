from decimal import Decimal
from enum import Enum

from sqlalchemy import Column
from sqlalchemy import Enum as SqlEnum
from sqlalchemy import ForeignKey, Integer
from sqlalchemy.orm import relationship

from app.database import Base
from app.models import BALANCE_TYPE, BaseModel


class ChangeTrigger(Enum):
    unknown: int = 0
    deposit: int = 1
    withdraw: int = 2
    transfer: int = 3
    transaction_deleted: int = 4


class Balance(Base, BaseModel):
    __tablename__ = "balances"

    value: Decimal = Column("value", BALANCE_TYPE, default=0, nullable=False)
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    changed_by: ChangeTrigger = Column(
        SqlEnum(ChangeTrigger),
        nullable=False,
        default=ChangeTrigger.unknown,
    )
    user = relationship(
        "User",
        back_populates="balances",
    )
