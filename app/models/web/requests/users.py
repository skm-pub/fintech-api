from app.models.web import BaseIN, BaseModel


class UserIN(BaseModel):
    id: int | None

    name: str
    surname: str
    middle_name: str | None

    is_blocked: bool
    is_system: bool


class UserCreateIN(BaseIN):
    data: UserIN


class UserUpdateIN(BaseIN):
    data: UserIN


class UserDeleteIN(BaseIN):
    id: int
