from decimal import Decimal

from app.models.web import BaseModel


class BalanceIN(BaseModel):
    add: Decimal
