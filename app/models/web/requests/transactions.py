from decimal import Decimal

from app.models.web import BaseIN, BaseModel


class TransactionBodyIN(BaseModel):
    sender: int
    recepient: int
    value: Decimal

    class Config:
        json_encoders = {Decimal: str}


class TransactionDepositBodyIN(TransactionBodyIN):
    sender: None
    recepient: int


class TransactionWithdrawBodyIN(TransactionBodyIN):
    sender: int
    recepient: None


class TransactionCreateIN(BaseIN):
    data: TransactionBodyIN | TransactionDepositBodyIN | TransactionWithdrawBodyIN


class TransactionDeleteIN(BaseIN):
    id: int
