from fastapi_pagination import Page

from app.models.web import ResponseError, ResponseOK, TransactionBodyOUT


class TransactionOUT(ResponseOK):
    msg = "Transaction"
    data: TransactionBodyOUT


class TransactionNotFound(ResponseError):
    msg = "Transaction not found"


class TransactionListOUT(ResponseOK):
    data: Page[TransactionBodyOUT]
    msg = "List of transactions"


class TransactionListNotFound(ResponseError):
    data: Page
    msg = "Transactions not found"


class TransactionCreateOUT(ResponseOK):
    id: int
    msg = "Transaction created successfully"


class TransactionCreateErrorOUT(ResponseError):
    msg = "Transaction not created"


class TransactionDeleteOUT(ResponseOK):
    id: int
    msg = "Transaction deleted successfully"


class TransactionDeleteErrorOUT(ResponseError):
    msg = "Transaction not deleted"
