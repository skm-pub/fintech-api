from fastapi_pagination import Page

from app.models.web import ResponseError, ResponseOK, User


class UserBodyOUT(User):
    id: int


class UserOUT(ResponseOK):
    data: UserBodyOUT
    msg = "User"


class UserCreateOUT(ResponseOK):
    id: int
    msg = "User created successfully"


class UserUpdateOUT(ResponseOK):
    id: int
    msg = "User updated successfully"


class UserDeleteOUT(ResponseOK):
    id: int
    msg = "User deleted successfully"


class UserCreateErrorOUT(ResponseError):
    msg = "User not created"


class UserUpdateErrorOUT(ResponseError):
    msg = "User not updated"


class UserDeleteErrorOUT(ResponseError):
    msg = "User not deleted"


class UserListOUT(ResponseOK):
    data: Page[UserBodyOUT]
    msg = "List of users"


class UserListNotFound(ResponseError):
    data: Page
    msg = "Users not found"


class UserNotFound(ResponseError):
    msg = "User not found"
