from decimal import Decimal
from typing import Any

from pydantic import BaseModel as _BaseModel
from pydantic import Field

from app import TransactionStatus, TransactionType


class BaseModel(_BaseModel):
    class Config:
        @staticmethod
        def schema_extra(schema: dict, _):
            props = {}
            for k, v in schema.get("properties", {}).items():
                if not v.get("hidden", False):
                    props[k] = v
            schema["properties"] = props


class User(BaseModel):
    id: int | None

    name: str
    surname: str
    middle_name: str

    balance: Decimal

    is_blocked: bool
    is_system: bool

    class Config:
        json_encoders = {Decimal: str}


class Balance(BaseModel):
    id: int | None
    value: Decimal
    user: User

    class Config:
        json_encoders = {Decimal: str}


class BalanceWithoutUser(Balance):
    id: int
    user: Any | None = Field(hidden=True)


class BaseIN(BaseModel):
    data: Any | None


class BaseOUT(BaseModel):
    msg: str
    data: Any


class ResponseOK(BaseOUT):
    ...


class ResponseError(BaseOUT):
    data: Any


class TransactionBodyOUT(BaseModel):
    id: int
    sender: int
    recepient: int
    value: Decimal
    type: TransactionType
    status: TransactionStatus

    class Config:
        use_enum_values = True
        json_encoders = {Decimal: str}


User.update_forward_refs()
