from enum import Enum

from pydantic import BaseModel, BaseSettings


class Env(Enum):
    test: str = "test"
    debug: str = "debug"
    dev: str = "dev"
    qa: str = "qa"
    prod: str = "prod"


class LogSettings(BaseModel):
    path: str
    bugsnag_api_key: str


class MainSettings(BaseModel):
    env: Env
    log: LogSettings
    project_name: str
    backend_cors_origins: str
    token_header_name: str
    token: str


class DB(BaseModel):
    url_service: str
    url_main: str
    url_template: str
    url_celery: str
    redis_broker_url: str
    redis_result_backend: str
    celery_queue_name: str


class MainConfig(BaseSettings):
    main: MainSettings
    db: DB

    class Config:
        env_prefix = "FINTECH_API_"
        env_nested_delimiter = "__"
