from fastapi_pagination import Page, Params
from fastapi_pagination.ext.async_sqlalchemy import paginate
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.crud import AbstractCRUDFactory, DataBaseCRUD
from app.models.balances import Balance, ChangeTrigger
from app.models.transactions import Transaction, TransactionStatus, TransactionType
from app.models.users import User
from app.models.web.requests.transactions import TransactionBodyIN
from app.tasks import process_transaction


class TransactionCRUD(DataBaseCRUD):
    async def _get_system_account(self, session: AsyncSession) -> User | None:
        query = select(User).where(User.is_system == True).limit(1)
        result = await session.execute(query)

        try:
            (system_account,) = result.one()
        except NoResultFound:
            return None

        return system_account

    async def create(self, tran: TransactionBodyIN) -> int | bool:
        assert tran.value > 0, "Transaction value must be greater than zero"

        async with await self._session_maker as session:
            async with session.begin():
                _tran = Transaction()
                _tran.value = tran.value

                if tran.sender == 0:
                    tran.sender = None

                if tran.recepient == 0:
                    tran.recepient = None

                if not all([tran.sender, tran.recepient]):
                    system = await self._get_system_account(session)

                if tran.sender is None:
                    _tran.sender = system.id
                else:
                    _sender: User = await session.get(User, tran.sender)
                    if not _sender:
                        return False

                    assert not _sender.is_blocked, "Sender is blocked"

                    assert (
                        _sender.balance >= tran.value
                    ), "User balance lower than transaction value"

                    _tran.sender = _sender.id

                if tran.recepient is None:
                    _tran.recepient = system.id
                else:
                    _recepient: User = await session.get(User, tran.recepient)
                    if not _recepient:
                        return False

                    assert not _recepient.is_blocked, "Recepient is blocked"

                    _tran.recepient = _recepient.id

                match tran.sender is None, tran.recepient is None:
                    case False, False:
                        _tran.type = TransactionType.transfer
                    case True, False:
                        _tran.type = TransactionType.deposit
                    case False, True:
                        _tran.type = TransactionType.withdraw
                    case _:
                        return False

                session.add(_tran)

                await session.flush(
                    [
                        _tran,
                    ]
                )

                tran_id = _tran.id

        process_transaction.delay(tran_id, session.bind.url.database)

        return tran_id

    async def read(
        self, page: int = 1, size: int = 50, tran_id: int = None
    ) -> Page[Transaction]:
        query = select(Transaction)

        if tran_id is not None:
            query = query.where(Transaction.id == tran_id)

        async with await self._session_maker as session:
            page = await paginate(session, query, params=Params(size=size, page=page))

            return page

    async def read_one(self, tran_id: int) -> Transaction | None:
        page = await self.read(tran_id=tran_id)
        if not page.items:
            return

        return page.items[0]

    async def update(self, *args, **kwargs):
        raise NotImplementedError

    async def delete(self, tran_id: int) -> bool:
        async with await self._session_maker as session:
            async with session.begin():
                tran: Transaction = await session.get(Transaction, tran_id)

                if not tran:
                    return False

                sender: User = await session.get(User, tran.sender)
                if not sender.is_system:
                    sender.balance += tran.value
                    sender_balance = Balance(
                        user_id=sender.id,
                        value=sender.balance,
                        changed_by=ChangeTrigger.transaction_deleted,
                    )
                    session.add(sender_balance)

                recepient: User = await session.get(User, tran.recepient)
                if not recepient.is_system:
                    recepient.balance -= tran.value
                    recepient_balance = Balance(
                        user_id=recepient.id,
                        value=recepient.balance,
                        changed_by=ChangeTrigger.transaction_deleted,
                    )
                    session.add(recepient_balance)

                tran.status = TransactionStatus.deleted

                return True


class TransactionCRUDFactory(AbstractCRUDFactory):
    @classmethod
    def create(cls) -> TransactionCRUD:
        return TransactionCRUD()


tran_crud = TransactionCRUDFactory.create
