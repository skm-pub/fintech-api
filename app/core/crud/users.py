from fastapi_pagination import Page, Params
from fastapi_pagination.ext.async_sqlalchemy import paginate
from sqlalchemy import select, update
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import joinedload

from app.core.crud import AbstractCRUDFactory, DataBaseCRUD
from app.models.balances import Balance
from app.models.users import User
from app.models.web.requests.users import UserIN


class UserCRUD(DataBaseCRUD):
    async def create(self, user: UserIN) -> int | bool:
        async with await self._session_maker as session:
            async with session.begin():
                _user = User()
                _user.name = user.name
                _user.surname = user.surname
                _user.middle_name = user.middle_name
                _user.is_blocked = user.is_blocked
                _user.is_system = user.is_system

                balance = Balance()
                balance.value = 0

                _user.balances.append(balance)

                session.add(_user)

                await session.flush([_user, balance])

                return _user.id

    async def read(
        self, page: int = 1, size: int = 50, user_id: int = None
    ) -> Page[User]:
        query = select(User).options(joinedload(User.balances))

        if user_id is not None:
            query = query.where(User.id == user_id)

        async with await self._session_maker as session:
            page = await paginate(session, query, params=Params(size=size, page=page))

            return page

    async def read_one(self, user_id: int) -> User | None:
        page = await self.read(user_id=user_id)
        if not page.items:
            return

        return page.items[0]

    async def update(self, new: UserIN) -> int | None:
        async with await self._session_maker as session:
            async with session.begin():
                res = await session.execute(
                    update(User)
                    .where(User.id == new.id)
                    .values(
                        name=new.name,
                        surname=new.surname,
                        middle_name=new.middle_name,
                        is_blocked=new.is_blocked,
                    )
                    .returning(User.id)
                )

                try:
                    (updated_id,) = res.one()
                except NoResultFound:
                    updated_id = None
                finally:
                    return updated_id

    async def delete(self, user_id: int) -> bool:
        async with await self._session_maker as session:
            async with session.begin():
                user = await session.get(User, user_id)

                if not user:
                    return False

                await session.delete(user)

            return True


class UserCRUDFactory(AbstractCRUDFactory):
    @classmethod
    def create(cls) -> UserCRUD:
        return UserCRUD()


user_crud = UserCRUDFactory.create
