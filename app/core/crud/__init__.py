from abc import ABC, abstractmethod

from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import sessionmaker

from app.core.config import MainConfig
from app.database import EngineFactory


class AbstractCRUD(ABC):
    @abstractmethod
    async def create(self, *args, **kwargs):
        ...

    @abstractmethod
    async def read(self, *args, **kwargs):
        ...

    @abstractmethod
    async def update(self, *args, **kwargs):
        ...

    @abstractmethod
    async def delete(self, *args, **kwargs):
        ...


class DataBaseCRUD(AbstractCRUD):
    __config = MainConfig()
    __engine = None
    __maker = None

    @property
    async def _session_maker(self) -> sessionmaker:
        if not self.__engine:
            self.__engine = await EngineFactory().create()

        if not self.__maker:
            self.__maker = sessionmaker(class_=AsyncSession, bind=self.__engine)

        return self.__maker()

    @abstractmethod
    async def create(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    async def read(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    async def update(self, *args, **kwargs):
        raise NotImplementedError

    @abstractmethod
    async def delete(self, *args, **kwargs):
        raise NotImplementedError


class AbstractCRUDFactory(ABC):
    @abstractmethod
    async def create(self) -> AbstractCRUD:
        ...
