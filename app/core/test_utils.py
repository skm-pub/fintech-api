import pytest_asyncio
from fastapi.testclient import TestClient

from app.database import EngineFactory
from app.main import app, shutdown, startup


@pytest_asyncio.fixture
async def client() -> TestClient:
    engine_factory = EngineFactory()
    await engine_factory.create()

    app.on_startup = [startup]
    app.on_shutdown = [shutdown]

    with TestClient(app) as c:
        yield c

    await engine_factory.db_delete()
