import contextlib

from celery import Celery
from sqlalchemy import create_engine, pool, select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session, aliased, sessionmaker
from yarl import URL

from app import (
    Balance,
    ChangeTrigger,
    Transaction,
    TransactionStatus,
    TransactionType,
    User,
)
from app.core.config import MainConfig

config = MainConfig()
celery = Celery(
    config.main.project_name,
    broker=config.db.redis_broker_url,
    backend=config.db.redis_result_backend,
)


@contextlib.contextmanager
def make_session(db_name) -> Session:
    db_url = URL(config.db.url_celery).with_path(f"/{db_name}")
    session = sessionmaker(
        bind=create_engine(db_url.human_repr(), poolclass=pool.NullPool)
    )()
    yield session


@celery.task(
    name="process_transaction", queue=config.db.celery_queue_name, acks_late=True
)
def process_transaction(tran_id, db_name) -> bool:
    sender: User = aliased(User)
    recepient: User = aliased(User)

    with make_session(db_name) as session:
        with session.begin():
            query = (
                select(Transaction, sender, recepient)
                .join(sender, Transaction.sender == sender.id)
                .join(recepient, Transaction.recepient == recepient.id)
                .where(Transaction.id == tran_id)
                .with_for_update()
                .limit(1)
            )
            result = session.execute(query)
            try:
                (
                    tran,
                    sender,
                    recepient,
                ) = result.one()
            except NoResultFound:
                tran.status = TransactionStatus.error
                return False

            if sender.balance < tran.value and not sender.is_system:
                tran.status = TransactionStatus.error
                return False

            match tran.type:
                case TransactionType.deposit:
                    balance_trigger = ChangeTrigger.deposit
                case TransactionType.withdraw:
                    balance_trigger = ChangeTrigger.withdraw
                case TransactionType.transfer:
                    balance_trigger = ChangeTrigger.transfer
                case _:
                    balance_trigger = ChangeTrigger.unknown

            if not sender.is_system:
                sender.balance -= tran.value
                sender_balance = Balance(
                    user_id=sender.id,
                    value=sender.balance,
                    changed_by=balance_trigger,
                )
                session.add(sender_balance)

            if not recepient.is_system:
                recepient.balance += tran.value
                recepient_balance = Balance(
                    user_id=recepient.id,
                    value=recepient.balance,
                    changed_by=balance_trigger,
                )
                session.add(recepient_balance)

            tran.status = TransactionStatus.success

            return True
