import functools
from uuid import uuid4

import sqlalchemy
from sqlalchemy import pool
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from yarl import URL

from app.core.config import Env, MainConfig

CONVENTION = {
    "all_column_names": lambda constraint, table: "_".join(
        [column.name for column in constraint.columns.values()]
    ),
    "ix": "ix__%(table_name)s__%(all_column_names)s",
    "uq": "uq__%(table_name)s__%(all_column_names)s",
    "ck": "ck__%(table_name)s__%(constraint_name)s",
    "fk": "fk__%(table_name)s__%(all_column_names)s__" "%(referred_table_name)s",
    "pk": "pk__%(table_name)s",
}

config = MainConfig()
metadata = sqlalchemy.MetaData(naming_convention=CONVENTION)
Base = declarative_base()


class EngineFactory:
    _instance: "EngineFactory" = None
    __config: MainConfig = MainConfig()

    __engine_service: AsyncEngine | None = None
    __engine_main: AsyncEngine | None = None

    @staticmethod
    def only_on_test_env(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            if EngineFactory.__config.main.env != Env.test:
                raise RuntimeError("Method available only on the TEST environment")
            return await func(*args, **kwargs)

        return wrapper

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls)
            cls._instance.__engine_service = create_async_engine(
                EngineFactory.__config.db.url_service,
                poolclass=pool.NullPool,
                execution_options={"isolation_level": "AUTOCOMMIT"},
            )

        return cls._instance

    async def create(self):
        if not self.__engine_main:
            if self.__config.main.env == Env.test:
                self.__engine_main = await self.tmp_db_create()
            else:
                self.__engine_main = create_async_engine(
                    self.__config.db.url_main,
                    poolclass=pool.NullPool,
                )

        return self.__engine_main

    def __create_service_session(
        self, autocommit=False, autoflush=False, expire_on_commit=False
    ) -> sessionmaker:
        return sessionmaker(
            class_=AsyncSession,
            autocommit=autocommit,
            autoflush=autoflush,
            expire_on_commit=expire_on_commit,
            bind=self.__engine_service,
        )

    @only_on_test_env
    async def tmp_db_create(
        self, autocommit=False, autoflush=False, expire_on_commit=False
    ):
        tpl_db = URL(self.__config.db.url_template)
        tpl_db_name = tpl_db.path.lstrip("/")
        new_db_name = uuid4().hex

        new_db_url = tpl_db.with_path(f"/{new_db_name}")

        sm = self.__create_service_session(autocommit, autoflush, expire_on_commit)
        async with sm() as session:
            await session.execute(
                f'CREATE DATABASE "{new_db_name}" TEMPLATE {tpl_db_name}'
            )

        return create_async_engine(
            new_db_url.human_repr(),
            poolclass=pool.NullPool,
        )

    @only_on_test_env
    async def db_delete(
        self, autocommit=False, autoflush=False, expire_on_commit=False
    ):
        if self.__engine_main is None:
            return True

        sm = self.__create_service_session(autocommit, autoflush, expire_on_commit)
        async with sm() as session:
            name = self.__engine_main.engine.url.database
            await session.execute(
                f"SELECT pid, pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '{name}'"
            )
            await session.execute(f'DROP DATABASE "{name}"')
            self.__engine_main = None
